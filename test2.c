#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"

/* program to make a steganograph from a text file into bitmap file */
int
main (int argc, char **argv)
{
	Bitmap *bmp;
	FILE *fp;
	char *data;
	size_t size;
	char c;
	int i;

	if (argc != 3) {
		printf("Usage: %s <txtfile> <bmpfile>\n", argv[0]);
		return 1;
	}

	if ((fp = fopen(argv[1], "rt")) == NULL) {
		printf("Error: Cannot open %s for reading.\n", argv[1]);
		return 1;
	}
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);
	if ((data = malloc(size)) == NULL) {
		printf("Error: Cannot allocate memory for text file.\n");
		fclose(fp);
		return 1;
	}
	i = 0;
	while((c = fgetc(fp)) != EOF)
		data[i++] = c;
	data[i] = '\0';
	fclose(fp);
	bmp = load_bitmap(argv[2]);
	if (get_last_error_bitmap() != BMP_NO_ERROR) {
		free(data);
		return 1;
	}
	encode_steganograph(bmp, data);
	if (get_last_error_bitmap() != BMP_NO_ERROR) {
		free(data);
		return 1;
	}
	free(data);
	write_bitmap(bmp, "out.bmp");
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
	destroy_bitmap(bmp);
	return 0;
}
