# Simple bitmap library

CFLAGS  := -std=c89 -g -Wall -Wextra -Wno-unused-parameter -I./common
LDFLAGS :=

OBJ1 := test1.o common/bitmap.o
EXE1 := test1
OBJ2 := test2.o common/bitmap.o
EXE2 := test2
OBJ3 := test3.o common/bitmap.o
EXE3 := test3
OBJ4 := test4.o common/bitmap.o
EXE4 := test4
OBJ5 := test5.o common/bitmap.o
EXE5 := test5
OBJ6 := test6.o common/bitmap.o
EXE6 := test6
OBJ7 := test7.o common/bitmap.o
EXE7 := test7
OBJ8 := test8.o common/bitmap.o
EXE8 := test8

# Custom libraries
LOBJ := common/bitmap.o

LIB1 := PRS_Bitmap.so
EXES := $(EXE1) $(EXE2) $(EXE3) $(EXE4) $(EXE5) $(EXE6) $(EXE7) $(EXE8)

.PHONY: all clean
all: $(EXES)

.o:
	$(CC) $(CFLAGS) -c $< -o $@ $(LDFLAGS)

$(LIB1): $(LOBJ)
	$(CC) $(CFLAGS) -fPIC -shared $^ -o $@ $(LDFLAGS)

$(EXE1): $(OBJ1)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE2): $(OBJ2)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE3): $(OBJ3)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE4): $(OBJ4)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE5): $(OBJ5)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE6): $(OBJ6)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE7): $(OBJ7)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

$(EXE8): $(OBJ8)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

install:
	@mkdir -p $(DESTDIR)/$(PREFIX)/{include,lib}
	@install common/bitmap.h $(DESTDIR)/$(PREFIX)/include
	@install $(LIB1) $(DESTDIR)/$(PREFIX)/lib

uninstall:
	@rm -f $(DESTDIR)/$(PREFIX)/include/bitmap.h
	@rm -f $(DESTDIR)/$(PREFIX)/lib/$(LIB1)

clean:
	rm -f *.o common/*.o $(LIB1) $(EXES) *.bmp *.so
