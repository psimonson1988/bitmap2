#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"

/* program to decode a steganograph bitmap */
int
main (int argc, char **argv)
{
	Bitmap *bmp;
	char *data;

	if (argc != 2) {
		printf("Usage: %s <bmpfile>\n", argv[0]);
		return 1;
	}

	bmp = load_bitmap(argv[1]);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
	data = decode_steganograph(bmp);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;
	printf(" ***************\n"
		" * Hidden Text *\n"
		" ***************\n%s\n", data);
	destroy_bitmap(bmp);
	free(data);
	return 0;
}
