#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"

/* resize_bitmap:  resize a bitmap image; by a factor */
Bitmap*
resize_bitmap (Bitmap *bmp, const int factor)
{
	Bitmap *bmp2;
	int width,height;
	int x,y;
	float xf,yf;

	if ((factor > 0 && factor < 5)
		|| (factor > -5 && factor < 0)) {
		if (factor > -5 && factor < 0) {
			width = bmp->info.width/-factor;
			height = bmp->info.height/-factor;
		} else {
			width = bmp->info.width*factor;
			height = bmp->info.height*factor;
		}
	} else {
		return NULL;
	}

	bmp2 = create_bitmap(width, height);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return NULL;

	xf = (factor < 0) ? (float)(bmp->info.width/width) : (float)(width/bmp->info.width);
	yf = (factor < 0) ? (float)(bmp->info.height/height) : (float)(height/bmp->info.height);
	for(y=0; y<height; y++)
		for(x=0; x<width; x++) {
			Color pixel;

			if (yf > 1 || xf > 1) {
				get_pixel_bitmap(bmp, y/yf, x/xf, &pixel);
				set_pixel_bitmap(bmp2, y/yf, x/xf, pixel);
			} else {
				get_pixel_bitmap(bmp, y*yf, x*xf, &pixel);
				set_pixel_bitmap(bmp2, y*yf, x*xf, pixel);
			}
		}
	destroy_bitmap(bmp);
	return bmp2;
}

/* program to resize a bitmap file */
int
main (int argc, char **argv)
{
	Bitmap *bmp;

	if (argc != 3) {
		printf("Usage: %s <bmpfile> <factor>\n\tfactor=[0-100]\n",
				argv[0]);
		return 1;
	}
	bmp = load_bitmap(argv[1]);
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;

	bmp = resize_bitmap(bmp, atoi(argv[2]));
	write_bitmap(bmp, "out.bmp");
	if (get_last_error_bitmap() != BMP_NO_ERROR)
		return 1;

	destroy_bitmap(bmp);
	return 0;
}
